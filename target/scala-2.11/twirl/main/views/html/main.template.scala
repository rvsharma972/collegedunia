
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object main_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class main extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String)(content: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.32*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>

<html>
    <head>
        <title>College Dunia</title>
        <link rel="stylesheet" href="/assets/stylesheets/material.css">
        <link rel="stylesheet" href="/assets/stylesheets/mystyle.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
            <!-- Always shows a header, even in smaller screens. -->
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
            <header class="mdl-layout__header">
                <div class="mdl-layout__header-row">
                        <!-- Title -->
                    <span class="mdl-layout-title">College Dunia</span>
                        <!-- Add spacer, to align navigation to the right -->
                    <div class="mdl-layout-spacer"></div>
                        <!-- Navigation. We hide it in small screens. -->
                    <nav class="mdl-navigation mdl-layout--large-screen-only">
                        <a class="mdl-navigation__link" href="#profile-outer-container"><span>ABOUT</span></a>
                        <a class="mdl-navigation__link" href="#"><span>SKILLS</span></a>
                        <a class="mdl-navigation__link" href="#profile-outer-container"><span>PORTFOLIO</span></a>
                        <a class="mdl-navigation__link" href="#experience-left-card-container"><span>EXPERIENCE</span></a>
                        <a class="mdl-navigation__link" href="#"><span>REFERENCE</span></a>
                        <a class="mdl-navigation__link" href="#"><span>BLOG</span></a>
                        <a class="mdl-navigation__link" href="#"><span>CALENDER</span></a>
                        <a class="mdl-navigation__link" href="#contact-container"><span>CONTACT</span></a>
                    </nav>
                </div>
            </header>
            <div class="mdl-layout__drawer" for="demo-menu-lower-right">
                <span class="mdl-layout-title__drawer">College Dunia</span>
                <nav class="mdl-navigation">
                    <a class="mdl-navigation__link" href="#">ABOUT</a>
                    <a class="mdl-navigation__link" href="#">SKILLS</a>
                    <a class="mdl-navigation__link" href="#">PORTFOLIO</a>
                    <a class="mdl-navigation__link" href="#">EXPERIENCE</a>
                    <a class="mdl-navigation__link" href="#">REFERENCE</a>
                    <a class="mdl-navigation__link" href="#">BLOG</a>
                    <a class="mdl-navigation__link" href="#">CALENDER</a>
                    <a class="mdl-navigation__link" href="#">CONTACT</a>
                </nav>
            </div>
            <main class="mdl-layout__content">
                <div class="page-content">
                    <div class="mdl-layout" id="profile-outer-container">
                        <div class="mdl-cell mdl-cell--8-col mdl-cell--2-offset-desktop mdl-cell--12-col-tablet">
                            <div class="mdl-grid mdl-shadow--2dp" id="profile-main-container" >
                                <div class="mdl-cell mdl-cell--5-col mdl-cell--4-col-tablet mdl-cell--12-col-phone" id="profile-picture-container">
                                    <div class="vertical-align-middle" id="profile-picture" style="text-align: center;padding: 2%">
                                        <img src="/assets/images/pp.svg" style="width: 70%;margin-top: 10%">
                                    </div>
                                </div>
                                <div class="mdl-cell mdl-cell--7-col mdl-cell--4-col-tablet mdl-cell--12-col-phone" id="profile-details-container">
                                    <div class="mdl-cell mdl-cell--2-col-tablet" id="profile-picture-container">
                                        <div id="profile-picture"></div>
                                    </div>
                                    <div id="hello-tag">
                                        <font style="color:#fff">
                                            <b>HELLO</b>
                                        </font>
                                    </div>
                                    <div>
                                        <font size="6" style="font-weight: 200;">I'm</font><font size="6" style="font-weight: 600"> Robert Smith</font>
                                    </div>
                                    <div>
                                        <font>Developer and businessman</font>
                                    </div>
                                    <hr>
                                    <div class="mdl-grid" id="profile-age-container">
                                        <div class="mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" id="details-label">
                                            <font>AGE</font>
                                        </div>
                                        <div class="mdl-cell--8-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" id="details-text">
                                            <font>29</font>
                                        </div>
                                    </div>
                                    <div class="mdl-grid" id="profile-address-container">
                                        <div class="mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" id="details-label">
                                            <font>ADDRESS</font>
                                        </div>
                                        <div class="mdl-cell--8-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" id="details-text">
                                            <font>Some Address</font>
                                        </div>
                                    </div>
                                    <div class="mdl-grid" id="profile-email-container">
                                        <div class="mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" id="details-label">
                                            <font>E-MAIL</font>
                                        </div>
                                        <div class="mdl-cell--8-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" id="details-text">
                                            <font>some@some.com</font>
                                        </div>
                                    </div>
                                    <div class="mdl-grid" id="profile-phone-container">
                                        <div class="mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" id="details-label">
                                            <font>PHONE</font>
                                        </div>
                                        <div class="mdl-cell--8-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" id="details-text">
                                            <font>788-888-6222</font>
                                        </div>
                                    </div>
                                    <div class="mdl-grid" id="profile-freelance-container">
                                        <div class="mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--2-col-phone"" id="details-label">
                                            <font>FREELANCE</font>
                                        </div>
                                        <div class="mdl-cell--8-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" id="details-text">
                                            <font>Available</font>
                                        </div>
                                    </div>
                                    <div class="mdl-grid" id="profile-freelance-container">
                                        <div class="mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" id="details-label-on-vacation">
                                            <font><span>On Vacation</span></font>
                                        </div>
                                        <div class="mdl-cell--8-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" id="details-text">
                                            <i class="material-icons vertical-align-middle" style="font-size: 18px">today</i>
                                            <font>till April 15,2016</font>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mdl-grid mdl-shadow--2dp" id="social-media-links-main-container">
                                <div class="mdl-cell mdl-cell--1-col mdl-cell--3-offset-desktop mdl-cell--1-offset-tablet mdl-cell--1-col-tablet mdl-cell--1-col-phone"><img src="/assets/images/facebook-logo.svg" height="15px"></div>
                                <div class="mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone"><img src="/assets/images/twitter-logo.svg" height="15px"></div>
                                <div class="mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone"><img src="/assets/images/linkedin-logo.svg" height="15px"></div>
                                <div class="mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone"><img src="/assets/images/gplus-logo.svg" height="15px"></div>
                                <div class="mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone"><img src="/assets/images/dribble-logo.svg" height="15px"></div>
                                <div class="mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone"><img src="/assets/images/instagram-logo.svg" height="15px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="mdl-layout" id="basic-details-outer-container">
                        <div class="mdl-grid">
                            <div class="mdl-cell mdl-cell--12-col">
                                    <div id="mdl-grid">
                                        <center>
                                            <div class="mdl-cell mdl-cell--2-col" id="download-resume">
                                                <font>DOWNLOAD RESUME</font>
                                            </div>
                                        </center>
                                    </div>
                            </div>
                            <div class="mdl-cell mdl-cell--12-col">
                                <center><h3 style="font-weight: 500;color: #37474f">Work Experience</h3></center>
                                <div class="mdl-grid" style="text-align: center;padding: 0">
                                    <div class="mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet" id="experience-left-card-container">
                                        <div>
                                            <div class="mdl-shadow--2dp" id="experience-card-left">
                                                <div id="timeline-year"><b>2014 - 2016</b></div>
                                                <div id="timeline-title">PINEAPPLE</div>
                                                <div id="timeline-post">FULL STACK DEVELOPER</div>
                                                <div id="timeline-description">I was responsible for working on a range of projects, designing
                                                    appeling websites and interacting on a daily basis with graphic designers, back-end developers and marketers.</div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="mdl-shadow--2dp" id="experience-card-left">
                                                <div id="timeline-year"><b>2014 - 2016</b></div>
                                                <div id="timeline-title">PINEAPPLE</div>
                                                <div id="timeline-post">FULL STACK DEVELOPER</div>
                                                <div id="timeline-description">I was responsible for working on a range of projects, designing
                                                    appeling websites and interacting on a daily basis with graphic designers, back-end developers and marketers.</div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="mdl-shadow--2dp" id="experience-card-left">
                                                <div id="timeline-year"><b>2014 - 2016</b></div>
                                                <div id="timeline-title">PINEAPPLE</div>
                                                <div id="timeline-post">FULL STACK DEVELOPER</div>
                                                <div id="timeline-description">I was responsible for working on a range of projects, designing
                                                    appeling websites and interacting on a daily basis with graphic designers, back-end developers and marketers.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="timeline-border"></div>
                                    <div class="mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet">
                                        <div id="right-card">
                                            <div class="mdl-shadow--2dp" id="experience-card-right">
                                                <div id="timeline-year"><b>2014 - 2016</b></div>
                                                <div id="timeline-title">PINEAPPLE</div>
                                                <div id="timeline-post">FULL STACK DEVELOPER</div>
                                                <div id="timeline-description">I was responsible for working on a range of projects, designing
                                                appeling websites and interacting on a daily basis with graphic designers, back-end developers and marketers.</div>
                                            </div>
                                            <div class="mdl-shadow--2dp" id="experience-card-right">
                                                <div id="timeline-year"><b>2014 - 2016</b></div>
                                                <div id="timeline-title">PINEAPPLE</div>
                                                <div id="timeline-post">FULL STACK DEVELOPER</div>
                                                <div id="timeline-description">I was responsible for working on a range of projects, designing
                                                    appeling websites and interacting on a daily basis with graphic designers, back-end developers and marketers</div>
                                            </div>
                                            <div class="mdl-shadow--2dp" id="experience-card-right">
                                                <div id="timeline-year"><b>2014 - 2016</b></div>
                                                <div id="timeline-title">PINEAPPLE</div>
                                                <div id="timeline-post">FULL STACK DEVELOPER</div>
                                                <div id="timeline-description">I was responsible for working on a range of projects, designing
                                                    appeling websites and interacting on a daily basis with graphic designers, back-end developers and marketers</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mdl-cell mdl-cell--12-col">
                                <center><h3 style="font-weight: 500;color: #37474f">Contact Me</h3></center>
                                <div class="mdl-grid" style="padding: 0">
                                    <div class="mdl-cell--4-col mdl-cell--2-offset-desktop mdl-shadow--2dp" id="contact-container">
                                        <form action="#">
                                            <h5>Feel free to contact me</h5>
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                <input class="mdl-textfield__input" type="text" id="name">
                                                <label class="mdl-textfield__label" for="name">NAME</label>
                                            </div>
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                <input class="mdl-textfield__input" type="text" id="email">
                                                <label class="mdl-textfield__label" for="email">EMAIL</label>
                                            </div>
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                <input class="mdl-textfield__input" type="text" id="subject">
                                                <label class="mdl-textfield__label" for="subject">SUBJECT</label>
                                            </div>
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                <textarea class="mdl-textfield__input" type="text" rows="4" id="message"></textarea>
                                                <label class="mdl-textfield__label" for="message">MESSAGE</label>
                                            </div>
                                            <div class>
                                                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                                                    SEND
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="mdl-cell--4-col  mdl-shadow--2dp" id="address-container">
                                        <div id="address-main-container">
                                            <div class="mdl-grid">
                                                <div class="mdl-cell--4-col" id="contact-label">
                                                    <font>ADDRESS</font>
                                                </div>
                                                <div class="mdl-cell--8-col" id="contact-text">
                                                    <font>Some Address</font>
                                                </div>
                                            </div>
                                            <div class="mdl-grid">
                                                <div class="mdl-cell--4-col" id="contact-label">
                                                    <font>PHONE</font>
                                                </div"""),
format.raw(""">
                                                <div class="mdl-cell--8-col" id="contact-text">
                                                    <font>8888-888-888</font>
                                                </div>
                                            </div>
                                            <div class="mdl-grid">
                                                <div class="mdl-cell--4-col" id="contact-label">
                                                    <font>E-MAIL</font>
                                                </div>
                                                <div class="mdl-cell--8-col" id="contact-text">
                                                    <font>some@some.com</font>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mdl-grid" style="padding:0">
                                                <div id="map"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mdl-cell mdl-cell--8-col mdl-cell--2-offset-desktop mdl-cell--2-offset-tablet">
                            <div class="mdl-grid">
                                <div class="mdl-cell mdl-cell--1-col mdl-cell--3-offset-desktop"><img src="/assets/images/footer/facebook-logo.svg" height="15px"></div>
                                <div class="mdl-cell mdl-cell--1-col "><img src="/assets/images/footer/twitter-logo.svg" height="15px"></div>
                                <div class="mdl-cell mdl-cell--1-col "><img src="/assets/images/footer/linkedin-logo.svg" height="15px"></div>
                                <div class="mdl-cell mdl-cell--1-col "><img src="/assets/images/footer/gplus-logo.svg" height="15px"></div>
                                <div class="mdl-cell mdl-cell--1-col "><img src="/assets/images/footer/dribble-logo.svg" height="15px"></div>
                                <div class="mdl-cell mdl-cell--1-col "><img src="/assets/images/footer/instagram-logo.svg" height="15px"></div>
                            </div>
                        </div>
                        </div>
                    </div>
            </main>
        </div>
        <script>

                function initMap() """),format.raw/*286.36*/("""{"""),format.raw/*286.37*/("""
                    """),format.raw/*287.21*/("""var uluru = """),format.raw/*287.33*/("""{"""),format.raw/*287.34*/("""lat: -25.363, lng: 131.044"""),format.raw/*287.60*/("""}"""),format.raw/*287.61*/(""";
                    var map = new google.maps.Map(document.getElementById('map'), """),format.raw/*288.83*/("""{"""),format.raw/*288.84*/("""
                        """),format.raw/*289.25*/("""zoom: 4,
                        center: uluru
                    """),format.raw/*291.21*/("""}"""),format.raw/*291.22*/(""");
                    var marker = new google.maps.Marker("""),format.raw/*292.57*/("""{"""),format.raw/*292.58*/("""
                        """),format.raw/*293.25*/("""position: uluru,
                        map: map
                    """),format.raw/*295.21*/("""}"""),format.raw/*295.22*/(""");
                """),format.raw/*296.17*/("""}"""),format.raw/*296.18*/("""

        """),format.raw/*298.9*/("""</script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjYqE0clwfMVc9Eofhozf0UUpSR52wVFM&callback=initMap"></script>
        <script src="/assets/javascripts/material.js"></script>
        <script src="/assets/javascripts/jquery-1.9.0.min.js"></script>
        <script src="/assets/javascripts/mainJS.js"></script>
    </body>
</html>
"""))
      }
    }
  }

  def render(title:String,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title)(content)

  def f:((String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title) => (content) => apply(title)(content)

  def ref: this.type = this

}


}

/**/
object main extends main_Scope0.main
              /*
                  -- GENERATED --
                  DATE: Mon Feb 20 19:37:43 IST 2017
                  SOURCE: /home/hawk/collegeDunia/collegedunia/app/views/main.scala.html
                  HASH: 04c50a10cb8de630e5eb4fe88ac2f4e1a73f16b7
                  MATRIX: 748->1|873->31|901->33|23428->22513|23458->22514|23508->22535|23549->22547|23579->22548|23634->22574|23664->22575|23777->22659|23807->22660|23861->22685|23957->22752|23987->22753|24075->22812|24105->22813|24159->22838|24258->22908|24288->22909|24336->22928|24366->22929|24404->22939
                  LINES: 27->1|32->1|34->3|318->286|318->286|319->287|319->287|319->287|319->287|319->287|320->288|320->288|321->289|323->291|323->291|324->292|324->292|325->293|327->295|327->295|328->296|328->296|330->298
                  -- GENERATED --
              */
          