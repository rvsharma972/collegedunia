
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/hawk/collegeDunia/collegedunia/conf/routes
// @DATE:Mon Feb 20 14:04:16 IST 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
