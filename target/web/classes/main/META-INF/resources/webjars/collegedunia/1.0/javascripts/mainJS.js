/**
 * Created by hawk on 20/2/17.
 */
$(function(){
    $('a[href^="#"]').on('click',function (e) {

        var target = this.hash;
        var $target = $(target);

        $('html, body').stop().animate({
            scrollTop: $target.offset().top
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
        e.preventDefault();
    });
});